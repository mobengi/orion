-module(payinfo_data).
-export([payinfo_parse_data/2,
        save_payinfo/2,
        delete_payinfo/1,
        update_parse_data/1,
        update_payinfo/3]).

payinfo_parse_data(FacebookId, Post) ->

    PayPal = case lists:keyfind(<<"paypal_id">>, 1, Post) of
       {<<"paypal_id">>, JsonPayPal} ->
           JsonPayPal;
       false ->
           <<"">>;
       _->
           <<"">>
    end,
   
    BankAccount = case lists:keyfind(<<"bank_account">>, 1, Post) of
       {<<"bank_account">>, JsonBankAccount} ->
           JsonBankAccount;
       false ->
           <<"">>;
       _->
           <<"">>
    end,
    
    [
        FacebookId,
	PayPal,
	BankAccount
       
    ]. 

update_parse_data(Post) ->
    PayPal = case lists:keyfind(<<"paypal_id">>, 1, Post) of
       {<<"paypal_id">>, JsonPayPal} ->
           JsonPayPal;
       false ->
           []
   end,
   BankAccount = case lists:keyfind(<<"bank_account">>, 1, Post) of
       {<<"bank_account">>, JsonBankAccount} ->
           JsonBankAccount;
       false ->
           []
   end,
   [
       PayPal,
       BankAccount
   ]. 

save_payinfo(JsonData,Data) ->
   [
       FacebookId,
       PayPal,
       BankAccount
   ] = Data,
   riak_driver:set_bucket(FacebookId, <<"PayPal">>, PayPal),
   riak_driver:set_bucket(FacebookId, <<"BankAccount">>, BankAccount),
   riak_driver:set_bucket_json(FacebookId, <<"PayInfoJson">>, JsonData),
   JsonData.

save_json_payinfo_update(FacebookId,Items) ->

    [
        PayPal,
        BankAccount
    ] = Items,

    SaveData =
            [{<<"fb_id">>, FacebookId}] ++ 
            [{<<"paypal_id">>, PayPal}] ++
            [{<<"bank_account">>, BankAccount}],

    %%riak_driver:update_indexes(MealId,Items),
    SaveData.

update_payinfo(FacebookId, Data, JsonData) ->
    [ 
        PayPal,
        BankAccount
    ] = Data,
   
   SavePayPal = case riak_driver:check_data(FacebookId, <<"PayPal">>) of
       not_found->
               riak_driver:set_bucket(FacebookId, <<"PayPal">>, PayPal),
               PayPal;
           _->
               case riak_driver:check_data_value(FacebookId, <<"PayPal">>) of
                   PayPal ->
                       PayPal;
                   _->
                       case PayPal of
                           [] ->
                               riak_driver:check_data_value(FacebookId, <<"PayPal">>);
                           _->
                               riak_driver:update_data_value(FacebookId, <<"PayPal">>, PayPal),
                               PayPal
                       end
               end
   end,

   SaveBankAccount = case riak_driver:check_data(FacebookId, <<"BankAccount">>) of
       not_found ->
               riak_driver:set_bucket(FacebookId, <<"BankAccount">>, BankAccount),
               BankAccount;
           _->
               case riak_driver:check_data_value(FacebookId, <<"BankAccount">>) of
                   BankAccount->
                       BankAccount;
                   _->
                       case BankAccount of
                           [] ->
                               riak_driver:check_data_value(FacebookId, <<"BankAccount">>);
                           _->
                               riak_driver:update_data_value(FacebookId, <<"BankAccount">>, BankAccount),
                               BankAccount
                       end
               end
       end,



   UpdatedData = [SavePayPal, SaveBankAccount],
   JsonSave = save_json_payinfo_update(FacebookId, UpdatedData),
   riak_driver:update_data_value_json(FacebookId, <<"PayInfoJson">>,JsonSave),
   JsonData.

delete_payinfo(FacebookId) ->
    ok = riak_driver:delete_buckets(FacebookId, payinfo, [], <<"PayInfoJson">>).
