-module(meal_data).

-export([meal_parse_data/2,
        save_meal/2,
        delete_meal/1,
        update_parse_data/1,
        update_meal/5]).

meal_parse_data(RequiredData, Post) ->
  [Meal, TimeStamp, MealId, FacebookId] = RequiredData,
  Title = case lists:keyfind(<<"title">>, 1, Post) of
       {<<"title">>, JsonTitle} ->
           JsonTitle;
       false ->
           <<"">>;
       _->
           <<"">>
   end,
   Desc = case lists:keyfind(<<"description">>, 1, Post) of
       {<<"description">>, JsonDesc} ->
           JsonDesc;
       false ->
           <<"">>;
       _->
           <<"">>
   end,
   Ingredients = case lists:keyfind(<<"ingredients">>, 1, Post) of
       {<<"ingredients">>, JsonIngredients} ->
           JsonIngredients;
       false ->
           <<"">>;
       _->
           <<"">>
   end,
   Portion = case lists:keyfind(<<"portion">>, 1, Post) of
       {<<"portion">>, JsonPortion} ->
           JsonPortion;
       false ->
           <<"">>;
       _->
           <<"">>
   end,
   Price = case lists:keyfind(<<"price">>, 1, Post) of
       {<<"price">>, JsonPrice} ->
           JsonPrice;
       false ->
           <<"">>;
       _->
           <<"">>
   end,
   Available = case lists:keyfind(<<"available">>, 1, Post) of
       {<<"available">>, JsonAvailable} ->
           JsonAvailable;
       false ->
           <<"">>;
       _->
           <<"">>
   end,
   Delivery = case lists:keyfind(<<"delivery">>, 1, Post) of
       {<<"delivery">>, JsonDelivery} ->
           JsonDelivery;
       false ->
           <<"">>;
       _->
           <<"">>
   end, 
   [   Meal,
       MealId,
       FacebookId,
       TimeStamp,
       Title,
       Desc,
       Ingredients,
       Portion,
       Price,
       Available,
       Delivery
   ]. 

update_parse_data(Post) ->
    Title = case lists:keyfind(<<"title">>, 1, Post) of
       {<<"title">>, JsonTitle} ->
           JsonTitle;
       false ->
           []
   end,
   Desc = case lists:keyfind(<<"description">>, 1, Post) of
       {<<"description">>, JsonDesc} ->
           JsonDesc;
       false ->
           []
   end,
   Ingredients = case lists:keyfind(<<"ingredients">>, 1, Post) of
       {<<"ingredients">>, JsonIngredients} ->
           JsonIngredients;
       false ->
           []
   end,
   Portion = case lists:keyfind(<<"portion">>, 1, Post) of
       {<<"portion">>, JsonPortion} ->
           JsonPortion;
       false ->
           []
   end,
   Price = case lists:keyfind(<<"price">>, 1, Post) of
       {<<"price">>, JsonPrice} ->
           JsonPrice;
       false ->
           []
   end,
   Available = case lists:keyfind(<<"available">>, 1, Post) of
       {<<"available">>, JsonAvailable} ->
           JsonAvailable;
       false ->
           []
   end,
   Delivery = case lists:keyfind(<<"delivery">>, 1, Post) of
       {<<"delivery">>, JsonDelivery} ->
           JsonDelivery;
       false ->
           []
   end, 
    [
       Title,
       Desc,
       Ingredients,
       Portion,
       Price,
       Available,
       Delivery
   ]. 

save_meal(JsonData,Data) ->
   [   Meal,
       MealId,
       FacebookId,
       TimeStamp,
       Title,
       Desc,
       Ingredients,
       Portion,
       Price,
       Available,
       Delivery
   ] = Data,
   %%Check if different and update
    riak_driver:set_bucket(MealId, <<"Cook">>, FacebookId),
    riak_driver:set_bucket(MealId, <<"Created_At">>, TimeStamp),
    riak_driver:set_bucket(MealId, <<"Title">>, Title),
    riak_driver:set_bucket(MealId, <<"Description">>, Desc),
    riak_driver:set_bucket(MealId, <<"Ingredients">>, Ingredients),
    riak_driver:set_bucket(MealId, <<"Portion">>, Portion),
    riak_driver:set_bucket(MealId, <<"Price">>, Price),
    riak_driver:set_bucket(MealId, <<"Available">>, Available),
    riak_driver:set_bucket(MealId, <<"Delivery">>, Delivery),
   SearchIndexes = [
        {"cook",FacebookId},
        {"title",Title},
        {"description",Desc},
        {"ingredients",Ingredients},
        {"portion",Portion},
        {"price", Price},
        {"available",Available},
        {"delivery",Delivery},
        {"created", TimeStamp},
        {"kitchen", Meal},
        {"meal_id", MealId}
    ],

    DataFinal = JsonData ++ [{<<"meal_id">>,MealId}] ++ [{<<"created">>,TimeStamp}],

    riak_driver:set_bucket_indexed_json(<<"meals">>,
                                        MealId,
                                        DataFinal,
                                        SearchIndexes),
    DataFinal.


save_json_meal_update(MealId, TimeStamp, CookId, Items) ->

    [
        Title,
        Desc,
        Ingredients,
        Portion,
        Price,
        Available,
        Delivery
    ] = Items,

    ResponseData =
            [{<<"meal_id">>,MealId}] ++ 
            [{<<"created">>,TimeStamp}] ++
            [{<<"title">>, Title}] ++
            [{<<"description">>, Desc}] ++
            [{<<"ingredients">>, Ingredients}] ++
            [{<<"portion">>, Portion}] ++
            [{<<"price">>, Price}] ++
            [{<<"available">>, Available}] ++
            [{<<"delivery">>, Delivery}] ++
            [{<<"fb_id">>, CookId}],

    riak_driver:update_meal_indexes(Items, MealId),
    
    ResponseData.

update_meal(MealId, TimeStamp, CookId, Data, JsonData) ->
    [  Title,
       Desc,
       Ingredients,
       Portion,
       Price,
       Available,
       Delivery
   ] = Data,
    
   %%Check if different and update
    ResTitle = case riak_driver:check_data(MealId, <<"Title">>) of
       not_found->
               riak_driver:set_bucket(MealId, <<"Title">>, Title),
               Title;
           _->
               case riak_driver:check_data_value(MealId, <<"Title">>) of
                   Title ->
                       Title;
                   _->
                       case Title of
                           [] ->
                               riak_driver:check_data_value(MealId, <<"Title">>);
                           _->
                               riak_driver:update_data_value(MealId, <<"Title">>, Title),
                               Title
                       end
               end
   end,
   
   ResDesc = case riak_driver:check_data(MealId, <<"Description">>) of
       not_found->
               riak_driver:set_bucket(MealId, <<"Description">>, Desc),
               Desc;
           _->
               case riak_driver:check_data_value(MealId, <<"Description">>) of
                   Desc ->
                       Desc;
                   _->
                       case Desc of
                           [] ->
                               riak_driver:check_data_value(MealId, <<"Description">>);
                           _->
                               riak_driver:update_data_value(MealId, <<"Description">>, Desc),
                               Desc
                       end
               end
   end,
   
   ResIngredients = case riak_driver:check_data(MealId, <<"Ingredients">>) of
       not_found->
               riak_driver:set_bucket(MealId, <<"Ingredients">>, Ingredients),
               Ingredients;
           _->
               case riak_driver:check_data_value(MealId, <<"Ingredients">>) of
                   Ingredients ->
                       Ingredients;
                   _->
                       case Ingredients of
                           [] ->
                               riak_driver:check_data_value(MealId, <<"Ingredients">>);
                           _->
                               riak_driver:update_data_value(MealId, <<"Ingredients">>, Ingredients),
                               Ingredients
                       end
               end
   end,
   
   ResPortion = case riak_driver:check_data(MealId, <<"Portion">>) of
       not_found->
               riak_driver:set_bucket(MealId, <<"Portion">>, Portion),
               Portion;
           _->
               case riak_driver:check_data_value(MealId, <<"Portion">>) of
                   Portion ->
                       Portion;
                   _->
                       case Portion of
                           [] ->
                               riak_driver:check_data_value(MealId, <<"Portion">>);
                           _->
                               riak_driver:update_data_value(MealId, <<"Portion">>, Portion),
                               Portion
                       end     
               end
   end,

   ResPrice = case riak_driver:check_data(MealId, <<"Price">>) of
       not_found->
               riak_driver:set_bucket(MealId, <<"Price">>, Price),
               Price;
           _->
               case riak_driver:check_data_value(MealId, <<"Price">>) of
                   Price ->
                       Price;
                   _->
                       case Price of 
                           [] ->
                               riak_driver:check_data_value(MealId, <<"Price">>);
                           _->
                               riak_driver:update_data_value(MealId, <<"Price">>, Price),
                               Price
                       end
               end
   end,
   ResAvailable=  case riak_driver:check_data(MealId, <<"Available">>) of
       not_found->
               riak_driver:set_bucket(MealId, <<"Available">>, Available),
               Available;
           _->
               case riak_driver:check_data_value(MealId, <<"Available">>) of
                   Available ->
                       Available;
                   _->
                       case Available of 
                           [] ->
                               riak_driver:check_data_value(MealId, <<"Available">>);
                           _->
                               riak_driver:update_data_value(MealId, <<"Available">>, Available),
                               Available
                       end
               end
   end,
   ResDelivery = case riak_driver:check_data(MealId, <<"Delivery">>) of
       not_found->
               riak_driver:set_bucket(MealId, <<"Delivery">>, Delivery),
               Delivery;
           _->
               case riak_driver:check_data_value(MealId, <<"Delivery">>) of
                   Delivery ->
                       Delivery;
                   _->
                       case Delivery of
                           [] ->
                               riak_driver:check_data_value(MealId, <<"Delivery">>);
                           _->
                               riak_driver:update_data_value(MealId, <<"Delivery">>, Delivery),
                               Delivery
                       end
               end
   end,
   UpdatedData = [ResTitle,ResDesc,ResIngredients,ResPortion,ResPrice,ResAvailable,ResDelivery],
   JsonSave = save_json_meal_update(MealId, TimeStamp, CookId, UpdatedData),
   riak_driver:update_data_value_json(<<"meals">>, MealId ,JsonSave),
   ResponseFinal = JsonData ++ [{<<"meal_id">>,MealId}] ++ [{<<"created">>,TimeStamp}],
   ResponseFinal.

delete_meal(MealId) ->
    ok = riak_driver:delete_buckets(MealId,all,<<"meals">>, []).
