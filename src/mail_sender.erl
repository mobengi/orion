-module(mail_sender).

-include("const.hrl").
-compile(export_all).

mailer_params()->
  {ok, EndPoint} = application:get_env(cent, mail_endpoint),
  {ok, User} = application:get_env(cent, mail_user),
  {ok, Pass} = application:get_env(cent, mail_pass),
  {ok, Sender} = application:get_env(cent, mail_sender),
  {ok, SubjectOrder} = application:get_env(cent, mail_subject_mealorder),
  {ok, OrderText} = application:get_env(cent, mail_order_text1),
  {ok, OrderText2} = application:get_env(cent, mail_order_text2),
  {ok, SubjectOrder2} = application:get_env(cent, mail_subject_order),
  {EndPoint, User, Pass, Sender, SubjectOrder, OrderText, SubjectOrder2, OrderText2}.

prepare_ordermeal_body(MealId, To, MailUser, MailPass, Sender, Subject, Text) ->
    

    Desc = "Description: " ++ binary_to_list(riak_driver:check_data_value(MealId, ?KEY_MEALDESC)),
    Title = "Title: "++ binary_to_list(riak_driver:check_data_value(MealId, ?KEY_MEALTITLE)),
    Id = "Meal id: " ++ binary_to_list(MealId),

    BodyText = Text ++ "\n"
                    ++ Id ++ "\n"
                    ++ Title ++ "\n" 
                    ++ Desc,

    BodyParams = "api_user=" ++ MailUser ++
                 "&api_key=" ++ MailPass ++
                 "&to="     ++   To++
                 "&from="    ++  Sender ++
                 "&subject=" ++  Subject ++
                 "&text="    ++   BodyText,
    BodyParams.

prepare_purchasedmeal_body(MealId, Buyer, MailUser, MailPass, Sender, SubjectOrder, OrderText) ->
    CookId = riak_driver:check_data_value(MealId, ?KEY_MEALCOOKID),
    Cook =  " Cook: " ++ binary_to_list(riak_driver:check_data_value(CookId, ?KEY_FULLNAME)),
    
    Address =  binary_to_list(riak_driver:check_data_value(CookId, ?KEY_ADDRESS)),
    PostCode =  binary_to_list(riak_driver:check_data_value(CookId, ?KEY_POSTCODE)),
    Municipality = binary_to_list(riak_driver:check_data_value(CookId, ?KEY_MUNICIPALITY)),
    Country = binary_to_list(riak_driver:check_data_value(CookId, ?KEY_COUNTRY)),

    FullAddress = " Address: " ++ Address ++" "++ PostCode ++" "++ Municipality ++" "++ Country,

    PhoneNumber = binary_to_list(riak_driver:check_data_value(CookId, ?KEY_PHONENO)),
    Email = binary_to_list(riak_driver:check_data_value(CookId, ?KEY_EMAIL)),

    ContactInfo = " Phone No: " ++ PhoneNumber ++" Email: "++ Email,

    BodyParams = "api_user=" ++ MailUser ++
                 "&api_key=" ++ MailPass ++
                 "&to="     ++   binary_to_list(Buyer)++
                 "&from="    ++  Sender ++
                 "&subject=" ++   SubjectOrder ++
                 "&text="    ++ OrderText  ++ "\n" 
                              ++ Cook ++ "\n"
                              ++ FullAddress ++ "\n"
                              ++ ContactInfo,
    BodyParams.

send_email_order(MealId, To) ->
    {UrlMail, User, Pass, Sender, _ ,_ ,SubjectOrder, OrderText} = mailer_params(),
    BodyParams = prepare_ordermeal_body(MealId, To, User, Pass, Sender, SubjectOrder, OrderText),
    httpc:request(post,
        {UrlMail, [],
        "application/x-www-form-urlencoded",
        BodyParams
    }, [], []).

send_email_purchase(MealId, To)->
    {UrlMail, User, Pass, Sender, SubjectOrder, OrderText ,_ ,_} = mailer_params(),
    BodyParams = prepare_purchasedmeal_body(MealId, To, User, Pass, Sender, SubjectOrder, OrderText),
    httpc:request(post,
        {UrlMail, [],
        "application/x-www-form-urlencoded",
        BodyParams
    }, [], []).
