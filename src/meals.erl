-module(meals).

-include("const.hrl").
-export([get_meals/0,
         get_meals_with_cook/0,
         get_cook_meals/1]).


meal_cook_items(Item)->

    {<<"fb_id">>, _CookId} = lists:keyfind(<<"fb_id">>, 1, Item),
    {<<"meal_id">>, _MealId} = lists:keyfind(<<"meal_id">>, 1, Item),
    {<<"created">>, _Created} = lists:keyfind(<<"created">>, 1, Item),
    {<<"title">>, _Title} = lists:keyfind(<<"title">>, 1, Item),
    {<<"description">>, _Desc} = lists:keyfind(<<"description">>, 1, Item),
    {<<"ingredients">>, _Ing} = lists:keyfind(<<"ingredients">>, 1, Item),
    {<<"portion">>, _Por} = lists:keyfind(<<"portion">>, 1, Item),
    {<<"price">>, _Price} = lists:keyfind(<<"price">>, 1, Item),
    {<<"available">>, _Avail} = lists:keyfind(<<"available">>, 1, Item),
    {<<"delivery">>, _Deli} = lists:keyfind(<<"delivery">>, 1, Item),

    CookInfo = profile_data:get_full_profile(_CookId),

    ItemInfo = [{<<"meal_id">>, _MealId}]++
    [{<<"created">>, _Created}]++
    [{<<"title">>, _Title}]++
    [{<<"description">>, _Desc}]++
    [{<<"ingredients">>, _Ing}]++
    [{<<"portion">>, _Por}]++
    [{<<"price">>, _Price}]++
    [{<<"available">>, _Avail}]++
    [{<<"delivery">>, _Deli}]++
    [{<<"cook">>, CookInfo}],
    ItemInfo.

get_meals_with_cook()->
    case riak_driver:get_records("meals", "kitchen", binary, ?ALL_MEALS, "munchmeal") of
        {[],<<"end">>} ->
            Response = [{<<"meals_number">>,<<"0">>}],
            Response;
        {Meals, <<"end">>} ->
            MealsData = lists:foldl(fun(X,MealsList) ->
                        MealItem = riak_driver:get_bucket_json(<<"meals">>,X),
                        lists:append([MealsList,[MealItem]])
                end, [], Meals),
            MealsListResponse = lists:foldl(fun(Item,MealsRes) ->
                        {<<"fb_id">>, _Id} = lists:keyfind(<<"fb_id">>, 1, Item),
                        MealItemInfo  = meal_cook_items(Item),
                        lists:append([MealsRes,[MealItemInfo]])
                end, [], MealsData),
            MealsFinalListResponse = [{<<"meals_number">>,
                    list_to_binary(integer_to_list(length(MealsData)))}] ++ [{<<"meals">>,MealsListResponse}],
            Response =  unicode:characters_to_binary(jsx:term_to_json(MealsFinalListResponse)),
            Response
    end.

get_meals()->
    case riak_driver:get_records("meals", "kitchen", binary, ?ALL_MEALS, "munchmeal") of
        {[],<<"end">>} ->
            Response = [{<<"meals_number">>,<<"0">>}],
            Response;
        {Meals, <<"end">>} ->
            MealsData = lists:foldl(fun(X,MealsList) ->
                        MealItem = riak_driver:get_bucket_json(<<"meals">>,X),
                        lists:append([MealsList,[MealItem]])
                end, [], Meals),
            MealsListResponse = lists:foldl(fun(Item,MealsRes) ->
                        {<<"meal_id">>, _Id} = lists:keyfind(<<"meal_id">>, 1, Item),
                        lists:append([MealsRes,[Item]])
                end, [], MealsData),
            MealsFinalListResponse = [{<<"meals_number">>,
                    list_to_binary(integer_to_list(length(MealsData)))}] ++ [{<<"meals">>,MealsListResponse}],
            Response =  unicode:characters_to_binary(jsx:term_to_json(MealsFinalListResponse)),
            Response
    end.

get_cook_meals(CookId) ->
    case riak_driver:get_records("meals", "cook", binary, ?ALL_MEALS,binary_to_list(CookId)) of
        {[],<<"end">>} ->
            Response = [{<<"meals_number">>,<<"0">>}],
            Response;
        {Meals, <<"end">>} ->
            MealsData = lists:foldl(fun(X,MealsList) ->
                        MealItem = riak_driver:get_bucket_json(<<"meals">>,X),
                        lists:append([MealsList,[MealItem]])
                end, [], Meals),
             MealsListResponse = lists:foldl(fun(Item,MealsRes) ->
                        {<<"meal_id">>, _Id} = lists:keyfind(<<"meal_id">>, 1, Item),
                        lists:append([MealsRes,[Item]])
                 end, [], MealsData),
             MealsFinalListResponse = [{<<"meals_number">>,
                     list_to_binary(integer_to_list(length(MealsData)))}] ++ [{<<"meals">>,MealsListResponse}],
             Response =  unicode:characters_to_binary(jsx:term_to_json(MealsFinalListResponse)),
             Response
    end.
