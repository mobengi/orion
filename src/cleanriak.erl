-module(cleanriak).
-export([clean/0]).

clean()->
    {ok, Pid} = riak_driver:connect(),
    {ok, B} = riakc_pb_socket:list_buckets(Pid),

    lists:foreach(fun(X) ->
             {ok, K} = riakc_pb_socket:list_keys(Pid,X),
                lists:foreach(fun(Y) ->
                            case X of
                                <<"rekon">> -> ignore;
                                _->
                                    riakc_pb_socket:delete(Pid, X, Y)
                            end
                    end, K)
        end,B).

