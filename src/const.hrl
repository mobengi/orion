%%Profile bucket keys
-define(KEY_PROFILE, <<"ProfileJson">>).
-define(KEY_PHOTO, <<"PhotoInfoJson">>).
-define(KEY_PAYINFO, <<"PayInfoJson">>).
-define(KEY_SIGNUP, <<"RoleJson">>).

-define(KEY_POSTCODE, <<"PostCode">>).
-define(KEY_EMAIL,<<"Email">>).
-define(KEY_ADDRESS, <<"Address">>).
-define(KEY_PHONENO, <<"PhoneNumber">>).
-define(KEY_ROLES, <<"Roles">>).
-define(KEY_FULLNAME, <<"FullName">>).
-define(KEY_CREATED, <<"Created_At">>).
-define(KEY_MUNICIPALITY, <<"Municipality">>).
-define(KEY_COUNTRY, <<"Country">>).
-define(KEY_PHOTOPATH, <<"PhotoPath">>).
-define(KEY_PHOTODESC, <<"PhotoDesc">>).
-define(KEY_PAYPAL, <<"PayPal">>).
-define(KEY_BANKACC, <<"BankAccount">>).
-define(KEY_LOC, <<"GeoLocation">>).

%%Meal bucket keys
-define(KEY_MEALDESC, <<"Description">>).
-define(KEY_MEALPRICE, <<"Price">>).
-define(KEY_MEALAVAILABLE, <<"Available">>).
-define(KEY_MEALDELIVERY, <<"Delivery">>).
-define(KEY_MEALCOOKID, <<"Cook">>).
-define(KEY_MEALCREATED, <<"Created_At">>).
-define(KEY_MEALPORTION, <<"Portion">>).
-define(KEY_MEALTITLE, <<"Title">>).
-define(KEY_MEALINGREDIENTS, <<"Ingredients">>).

%Orders
-define(ORDERS, <<"orders">>).
-define(ORDER, <<"munchorder">>).
-define(KEY_ORDERBUYER, <<"Buyer">>).
-define(KEY_ORDERMEALID, <<"MealId">>).
-define(KEY_ORDERTIME, <<"OrderTime">>).
-define(KEY_ORDER, <<"OrderJson">>).

%% Temporary meal number retreive 
-define(ALL_MEALS, 1000000000).

% Not found json
-define(NOT_FOUND, [{<<"error">>, <<"not_found">>}]).


%%Order not processed 
-define(NOT_ORDER, [{<<"error">>, <<"not_processed">>}]).

% Status deleted
-define(DELETED, [{<<"status">>, <<"deleted">>}]).

%CROSS DOMAIN LIST
-define(DOMAINS_LIST, [<<"localhost">>, <<"dev.munch.kitchen">>, <<"dev.munch.com">>,<<"127.0.0.1">>]).


