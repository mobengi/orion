-module(utils).
-export([get_timestamp/0,
	format/1]).


get_timestamp() ->
    {MegaS, S, MicroS} = erlang:now(),
    Id = list_to_binary(
        integer_to_list(MegaS) ++
        integer_to_list(S) ++
        integer_to_list(MicroS)
    ),
    Id.

format(List) -> format(List, []).
format([], Results) -> Results;
format([H|T], Results) -> format(T, [json(H)|Results]).

json({_, Key, Content, Priority, Status}) ->
   {Key, [Content, Priority, Status]}.
