-module(profile_data).
-include("const.hrl").
-export([profile_parse_data/2,
        save_profile/2,
        sign_up/3,
        resolve_location/5,
        delete_profile/1,
        update_parse_data/1,
        get_full_profile/1,
        update_profile/3]).

get_full_profile(FacebookId) ->
    Profile1 = try                                                                  
                Result1 = riak_driver:get_bucket_json(FacebookId, ?KEY_PROFILE), 
                Result1
            catch
                error:_Reason ->
                    []
            end,

    Profile2 = try                                                                  
                Result2 = riak_driver:get_bucket_json(FacebookId, ?KEY_PAYINFO), 
                Result2
            catch
                error:_Error ->
                    []
            end, 
    Profile3 = try
                Result3 = riak_driver:get_bucket_json(FacebookId, ?KEY_PHOTO), 
                Result3
            catch
                error:_NotFound ->
                    []
            end,
    Profile4  = try
                Result4 = riak_driver:get_bucket_json(FacebookId, ?KEY_LOC),
                %%[{Lat,V1},{Lon, V2}] = Result4,
                %LatVal = list_to_binary(float_to_list(V1,[{decimals,7}])),
                %LonVal = list_to_binary(float_to_list(V2,[{decimals,7}])),
                %[{<<"geoloc">>, [{Lat,LatVal},{Lon, LonVal}]}]
                [{<<"geoloc">>, Result4}]
            catch
                error:_NotHere ->
                    []
            end,

    FullProfile =  Profile1 ++ Profile2 ++ Profile3 ++Profile4, 
    lists:usort(FullProfile).

sign_up(FacebookId, Role, Post) ->
    case riak_driver:check_data(FacebookId , ?KEY_CREATED) of
        not_found ->
            CreatedAt = utils:get_timestamp(),
            riak_driver:set_bucket(FacebookId, ?KEY_CREATED, CreatedAt),
            riak_driver:set_bucket(FacebookId, ?KEY_ROLES, Role),
            riak_driver:set_bucket_json(FacebookId, ?KEY_SIGNUP, Post),
            PostFinal = Post ++ [{<<"profile">>, <<"0">>}],
            PostFinal;
        _->
            case riak_driver:check_data(FacebookId, ?KEY_PROFILE) of
                not_found ->
                    PostFinal = Post ++ [{<<"profile">>, <<"0">>}],
                    PostFinal;
                _->
                    PostFinal = Post ++ [{<<"profile">>, <<"1">>}],
                    PostFinal
            end
    end.

profile_parse_data(FacebookId, Post) ->
  FullName = case lists:keyfind(<<"full_name">>, 1, Post) of
       {<<"full_name">>, JsonFullName} ->
           JsonFullName;
       false ->
           <<"">>;
       _->
           <<"">>
   end,
   Email = case lists:keyfind(<<"email">>, 1, Post) of
       {<<"email">>, JsonEmail} ->
           JsonEmail;
       false ->
           <<"">>;
       _->
           <<"">>
   end,
   PhoneNumber = case lists:keyfind(<<"phone_number">>, 1, Post) of
       {<<"phone_number">>, JsonPhoneNumber} ->
           JsonPhoneNumber;
       false ->
           <<"">>;
       _->
           <<"">>
   end,
   Address = case lists:keyfind(<<"address">>, 1, Post) of
       {<<"address">>, JsonAddress} ->
           JsonAddress;
       false ->
           <<"">>;
       _->
           <<"">>
   end,
   PostCode = case lists:keyfind(<<"postcode">>, 1, Post) of
       {<<"postcode">>, JsonPostCode} ->
           JsonPostCode;
       false ->
           <<"">>;
       _->
           <<"">>
   end,
   Country = case lists:keyfind(<<"country">>, 1, Post) of
       {<<"country">>, JsonCountry} ->
           JsonCountry;
       false ->
           <<"">>;
       _->
           <<"">>
   end,
   Municipality = case lists:keyfind(<<"municipality">>, 1, Post) of
       {<<"municipality">>, JsonMunicipality} ->
           JsonMunicipality;
       false ->
           <<"">>;
       _->
           <<"">>
   end, 
   [   
       FacebookId,
       FullName,
       Address,
       Email,
       PostCode,
       PhoneNumber,
       Country,
       Municipality
   ]. 

update_parse_data(Post) ->
    FullName = case lists:keyfind(<<"full_name">>, 1, Post) of
       {<<"full_name">>, JsonFullName} ->
           JsonFullName;
       false ->
           []
   end,
   Address = case lists:keyfind(<<"address">>, 1, Post) of
       {<<"address">>, JsonAddress} ->
           JsonAddress;
       false ->
           <<"">>
   end,
   Email = case lists:keyfind(<<"email">>, 1, Post) of
       {<<"email">>, JsonEmail} ->
           JsonEmail;
       false ->
           []
   end,
   PostCode = case lists:keyfind(<<"postcode">>, 1, Post) of
       {<<"postcode">>, JsonPostCode} ->
           JsonPostCode;
       false ->
           <<"">>
   end,
   PhoneNumber = case lists:keyfind(<<"phone_number">>, 1, Post) of
       {<<"phone_number">>, JsonPhoneNumber} ->
           JsonPhoneNumber;
       false ->
           []
   end,
   Country = case lists:keyfind(<<"country">>, 1, Post) of
       {<<"country">>, JsonCountry} ->
           JsonCountry;
       false ->
           <<"">>
   end,
   Municipality = case lists:keyfind(<<"municipality">>, 1, Post) of
       {<<"municipality">>, JsonMunicipality} ->
           JsonMunicipality;
       false ->
           <<"">>
   end, 
    [   
       FullName,
       Address,
       Email,
       PostCode,
       PhoneNumber,
       Country,
       Municipality
   ].

save_profile(JsonData,Data) ->
   [   
       FacebookId,
       FullName,
       Address,
       Email,
       PostCode,
       PhoneNumber,
       Country,
       Municipality
   ] = Data,
   riak_driver:set_bucket(FacebookId, ?KEY_FULLNAME, FullName),
   riak_driver:set_bucket(FacebookId, ?KEY_EMAIL, Email),
   riak_driver:set_bucket(FacebookId, ?KEY_COUNTRY, Country),
   riak_driver:set_bucket(FacebookId, ?KEY_PHONENO, PhoneNumber),
   riak_driver:set_bucket(FacebookId, ?KEY_ADDRESS, Address),
   riak_driver:set_bucket(FacebookId, ?KEY_POSTCODE, PostCode),
   riak_driver:set_bucket(FacebookId, ?KEY_MUNICIPALITY, Municipality),
   riak_driver:set_bucket_json(FacebookId, ?KEY_PROFILE, JsonData),
   io:format("Printing ~p ~n",[FacebookId]),
   io:format("Printing ~p ~n",[Address]),
   io:format("Printing ~p ~n",[PostCode]),
   io:format("Printing ~p ~n",[Country]),
   io:format("Printing ~p ~n",[Municipality]),
   resolve_location(FacebookId,
       Address,
       PostCode,
       Country,
       Municipality),
   JsonData.


save_json_profile_update(FacebookId, Items) ->

    [
        FullName,
        Address,
        Email,
        PostCode,
        PhoneNumber,
        Country,
        Municipality
    ] = Items,

    SaveData =
            [{<<"fb_id">>, FacebookId}] ++ 
            [{<<"full_name">>,FullName}] ++
            [{<<"address">>, Address}] ++
            [{<<"email">>, Email}] ++
            [{<<"postcode">>, PostCode}] ++
            [{<<"phone_number">>, PhoneNumber}] ++
            [{<<"country">>, Country}] ++
            [{<<"municipality">>, Municipality}],

    %%riak_driver:update_indexes(MealId,Items),
    SaveData.

update_profile(FacebookId, Data, JsonData) ->
    [  
       FullName,
       Address,
       Email,
       PostCode,
       PhoneNumber,
       Country,
       Municipality
   ] = Data,
    
   %%Check if different and update
    SaveFullName = case riak_driver:check_data(FacebookId, ?KEY_FULLNAME) of
       not_found->
               riak_driver:set_bucket(FacebookId, ?KEY_FULLNAME, FullName),
               FullName;
           _->
               case riak_driver:check_data_value(FacebookId, ?KEY_FULLNAME) of
                   FullName ->
                       FullName;
                   _->
                       case FullName of
                           [] ->
                               riak_driver:check_data_value(FacebookId, ?KEY_FULLNAME);
                           _->
                               riak_driver:update_data_value(FacebookId, ?KEY_FULLNAME, FullName),
                               FullName
                       end
               end
   end,
   
   SaveAddress = case riak_driver:check_data(FacebookId, ?KEY_ADDRESS) of
       not_found->
               riak_driver:set_bucket(FacebookId, ?KEY_ADDRESS, Address),
               Address;
           _->
               case riak_driver:check_data_value(FacebookId, ?KEY_ADDRESS) of
                   Address ->
                       Address;
                   _->
                       case Address of
                           [] ->
                               riak_driver:check_data_value(FacebookId, ?KEY_ADDRESS);
                           _->
                               riak_driver:update_data_value(FacebookId, ?KEY_ADDRESS, Address),
                               Address
                       end
               end
   end,
   
   SaveEmail = case riak_driver:check_data(FacebookId, ?KEY_EMAIL) of
       not_found->
               riak_driver:set_bucket(FacebookId, ?KEY_EMAIL, Email),
               Email;
           _->
               case riak_driver:check_data_value(FacebookId, ?KEY_EMAIL) of
                   Email ->
                       Email;
                   _->
                       case Email of
                           [] ->
                               riak_driver:check_data_value(FacebookId, ?KEY_EMAIL);
                           _->
                               riak_driver:update_data_value(FacebookId, ?KEY_EMAIL, Email),
                               Email
                       end
               end
   end,
   
   SavePostCode = case riak_driver:check_data(FacebookId, ?KEY_POSTCODE) of
       not_found->
               riak_driver:set_bucket(FacebookId, ?KEY_POSTCODE, PostCode),
               PostCode;
           _->
               case riak_driver:check_data_value(FacebookId, ?KEY_POSTCODE) of
                   PostCode ->
                       PostCode;
                   _->
                       case PostCode of
                           [] ->
                               riak_driver:check_data_value(FacebookId, ?KEY_POSTCODE);
                           _->
                               riak_driver:update_data_value(FacebookId, ?KEY_POSTCODE, PostCode),
                               PostCode
                       end     
               end
   end,

   SavePhoneNumber = case riak_driver:check_data(FacebookId, ?KEY_PHONENO) of
       not_found->
               riak_driver:set_bucket(FacebookId, ?KEY_PHONENO, PhoneNumber),
               PhoneNumber;
           _->
               case riak_driver:check_data_value(FacebookId, ?KEY_PHONENO) of
                   PhoneNumber ->
                       PhoneNumber;
                   _->
                       case PhoneNumber of 
                           [] ->
                               riak_driver:check_data_value(FacebookId, ?KEY_PHONENO);
                           _->
                               riak_driver:update_data_value(FacebookId, ?KEY_PHONENO, PhoneNumber),
                               PhoneNumber
                       end
               end
   end,

   SaveCountry=  case riak_driver:check_data(FacebookId, ?KEY_COUNTRY) of
       not_found->
               riak_driver:set_bucket(FacebookId, ?KEY_COUNTRY, Country),
               Country;
           _->
               case riak_driver:check_data_value(FacebookId, ?KEY_COUNTRY) of
                   Country ->
                       Country;
                   _->
                       case Country of 
                           [] ->
                               riak_driver:check_data_value(FacebookId, ?KEY_COUNTRY);
                           _->
                               riak_driver:update_data_value(FacebookId, ?KEY_COUNTRY, Country),
                               Country
                       end
               end
   end,
   SaveMunicipality = case riak_driver:check_data(FacebookId, ?KEY_MUNICIPALITY) of
       not_found->
               riak_driver:set_bucket(FacebookId, ?KEY_MUNICIPALITY, Municipality),
               Municipality;
           _->
               case riak_driver:check_data_value(FacebookId, ?KEY_MUNICIPALITY) of
                   Municipality ->
                       Municipality;
                   _->
                       case Municipality of
                           [] ->
                               riak_driver:check_data_value(FacebookId, ?KEY_MUNICIPALITY);
                           _->
                               riak_driver:update_data_value(FacebookId, ?KEY_MUNICIPALITY, Municipality),
                               Municipality
                       end
               end
   end,
   resolve_location(FacebookId,
       SaveAddress,
       SavePostCode,
       SaveCountry,
       SaveMunicipality),
   io:format("Printing ~p ~n",[FacebookId]),
   io:format("Printing ~p ~n",[SaveAddress]),
   io:format("Printing ~p ~n",[SavePostCode]),
   io:format("Printing ~p ~n",[SaveCountry]),
   io:format("Printing ~p ~n",[SaveMunicipality]),
   UpdatedData = [SaveFullName,SaveAddress,SaveEmail,SavePostCode,SavePhoneNumber,SaveCountry,SaveMunicipality],
   JsonSave = save_json_profile_update(FacebookId, UpdatedData),
   riak_driver:update_data_value_json(FacebookId, ?KEY_PROFILE, JsonSave),
   JsonData.

resolve_location(FacebookId, Address, PostCode, Country, Municipality)->
   geocoder:start(),
   Res = try
       Value = <<Address/binary, PostCode/binary, Country/binary, Municipality/binary>>,
       geocoder:getLocation(binary_to_list(Value))
   catch
       error:_Error ->
           false
   end,
   geocoder:stop(),

   io:format("Printing ~p ~n",[Res]),

   case Res of
       false ->
           case riak_driver:check_data(FacebookId, ?KEY_LOC) of
               not_found ->
                   riak_driver:set_bucket_json(FacebookId, ?KEY_LOC, [{}]);
               _->
                   ignore
           end;
       _->
           case riak_driver:check_data(FacebookId, ?KEY_LOC) of
               not_found ->
                   riak_driver:set_bucket_json(FacebookId, ?KEY_LOC, Res);
               _->
                   case riak_driver:get_bucket_json(FacebookId, ?KEY_LOC) of
                       [{}]-> 
                           riak_driver:update_data_value_json(FacebookId, ?KEY_LOC, Res);
                       Res ->
                           ignore;
                       _ ->
                           riak_driver:set_bucket_json(FacebookId, ?KEY_LOC, Res)
                   end
           end
   end.

delete_profile(FacebookId) ->
    ok = riak_driver:delete_buckets(FacebookId, all, [], []).
