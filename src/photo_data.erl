-module(photo_data).
-export([photo_parse_data/2,
        save_photo/2,
        delete_photo/1,
        update_parse_data/1,
        update_photo/3]).

photo_parse_data(FacebookId, Post) ->

    PhotoDesc = case lists:keyfind(<<"photo_description">>, 1, Post) of
       {<<"photo_description">>, JsonPhotoDesc} ->
           JsonPhotoDesc;
       false ->
           <<"">>;
       _->
           <<"">>
    end,
   
    PhotoPath = case lists:keyfind(<<"photo_path">>, 1, Post) of
       {<<"photo_path">>, JsonPhotoPath} ->
           JsonPhotoPath;
       false ->
           <<"">>;
       _->
           <<"">>
    end,
    
    [
        FacebookId,
	PhotoDesc,
        PhotoPath
       
    ]. 

update_parse_data(Post) ->
    PhotoPath = case lists:keyfind(<<"photo_path">>, 1, Post) of
       {<<"photo_path">>, JsonPhotoPath} ->
           JsonPhotoPath;
       false ->
           []
   end,
   PhotoDesc = case lists:keyfind(<<"photo_description">>, 1, Post) of
       {<<"photo_description">>, JsonPhotoDesc} ->
           JsonPhotoDesc;
       false ->
           []
   end,
   [
       PhotoDesc,
       PhotoPath
   ]. 

save_photo(JsonData,Data) ->
   [
       FacebookId,
       PhotoDesc,
       PhotoPath
   ] = Data,
   riak_driver:set_bucket(FacebookId, <<"PhotoDesc">>, PhotoDesc),
   riak_driver:set_bucket(FacebookId, <<"PhotoPath">>, PhotoPath),
   riak_driver:set_bucket_json(FacebookId, <<"PhotoInfoJson">>, JsonData),
   JsonData.

save_json_photo_update(FacebookId,Items) ->

    [
        PhotoDesc,
        PhotoPath
    ] = Items,

    SaveData =
            [{<<"fb_id">>, FacebookId}] ++ 
            [{<<"photo_description">>, PhotoDesc}] ++
            [{<<"photo_path">>, PhotoPath}],

    %%riak_driver:update_indexes(MealId,Items),
    SaveData.

update_photo(FacebookId, Data, JsonData) ->
    [ 
        PhotoDesc,
        PhotoPath
    ] = Data,
   
   SavePhotoDesc = case riak_driver:check_data(FacebookId, <<"PhotoDesc">>) of
       not_found ->
               riak_driver:set_bucket(FacebookId, <<"PhotoDesc">>, PhotoDesc),
               PhotoDesc;
           _->
               case riak_driver:check_data_value(FacebookId, <<"PhotoDesc">>) of
                   PhotoDesc->
                       PhotoDesc;
                   _->
                       case PhotoDesc of
                           [] ->
                               riak_driver:check_data_value(FacebookId, <<"PhotoDesc">>);
                           _->
                               riak_driver:update_data_value(FacebookId, <<"PhotoDesc">>, PhotoDesc),
                               PhotoDesc
                       end
               end
       end,
       
   SavePhotoPath = case riak_driver:check_data(FacebookId, <<"PhotoPath">>) of
       not_found ->
                riak_driver:set_bucket(FacebookId, <<"PhotoPath">>, PhotoPath),
                PhotoPath;
            _->
                case riak_driver:check_data_value(FacebookId, <<"PhotoPath">>) of
                    PhotoPath ->
                        PhotoPath;
                    _->
                        case PhotoPath of
                            [] ->
                                riak_driver:check_data_value(FacebookId, <<"PhotoPath">>);
                            _->
                                riak_driver:update_data_value(FacebookId, <<"PhotoPath">>, PhotoPath),
                                PhotoPath
                    end
            end
    end,

   UpdatedData = [SavePhotoDesc, SavePhotoPath],
   JsonSave = save_json_photo_update(FacebookId, UpdatedData),
   riak_driver:update_data_value_json(FacebookId, <<"PhotoInfoJson">>,JsonSave),
   JsonData.

delete_photo(FacebookId) ->
    ok = riak_driver:delete_buckets(FacebookId, photo, [], <<"PhotoInfoJson">>).
