-module(request_order).
-compile({parse_transform, leptus_pt}).

-include("const.hrl").
%% Leptus callbacks
-export([init/3]).
-export([cross_domains/3]).
-export([terminate/4]).

%% Leptus routes
-export([post/3,
         get/3]).

%% Cross Domain Origin
%% It accepts any host for cross-domain requests
cross_domains(_Route, _Req, State) ->
    {?DOMAINS_LIST, State}.

%% Start
init(_Route, _Req, State) ->
   {ok, State}.

%% Test
get("/api", _Req, State) ->
    Body = [{<<"message">>, <<"Welcome to the MuncHoodAPI">>}],
    {200, {json, Body}, State}.

%% Order
post("/order/create", Req, State) ->
   %% Get POST body query string
   Post = leptus_req:body_qs(Req),
   Data = order_data:order_parse_data(Post),
   Response = order_data:save_order(Post, Data),
   {200, {json, Response}, State}.

%% End
terminate(_Reason, _Route, _Req, _State) ->
   ok.
