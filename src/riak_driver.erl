-module(riak_driver).

-export([start/2, stop/1,
        set_bucket/3,
        set_bucket_json/3,
        set_indexed_bucket/5,
        connect/0,
        get_buckets/0,
        disconnect/1,
        check_data/2,
        check_data_value/2,
        get_number/1,
        get_bucket_json/2,
        delete_bucket/2,
        get_records_next/6,
        get_records/5,
        update_meal_indexes/2,
        update_data_value/3,
        update_data_value_json/3,
        set_bucket_indexed_json/4,
        delete_buckets/4,
        update_check_bucket/5, 
        get_keys/1]).

-define(NODE_IP, "localhost").
-define(NODE_PORT, 8097).
%-define(NODE_PORT, 11087).


get_riak_port() ->
    case application:get_env(cent, riak_port) of
        {ok, Port} ->
            Port;
        _->
            8097
    end.

start(_Host, _Opts) ->
    ok.

stop(_Host) ->
    ok.

connect()->
    riakc_pb_socket:start_link(?NODE_IP, get_riak_port()).

disconnect(Pid)->
    riakc_pb_socket:stop(Pid).

delete_buckets(Bucket,
               _Req,
                _ExtraBucket, 
                _ExtraKey) ->
    {ok, Pid} = connect(),
    {ok, Keys} = riakc_pb_socket:list_keys(Pid, Bucket),
    case _Req of
        all ->
            lists:foreach(fun(K) ->
                        riakc_pb_socket:delete(Pid, Bucket, K)
                end, Keys);
        _->
            ignore
    end,
    case _ExtraBucket of
        [] -> ignore;
        _->
            riakc_pb_socket:delete(Pid, _ExtraBucket, Bucket)
    end,
    case _ExtraKey of
        [] -> ignore;
        _->    
            riakc_pb_socket:delete(Pid, Bucket, _ExtraKey)
    end,
    disconnect(Pid).

get_records(Bucket, Tag, Type, Amount, Criteria)->
    {ok, Pid } = connect(),
    {C, Result} =  case Type of 
                        binary -> riakc_pb_socket:get_index_eq(Pid, 
                                    list_to_binary(Bucket), 
                                    {binary_index, Tag},
                                    list_to_binary(Criteria),
                                    [{max_results, Amount}])
                    end,
    FinalResult = case C of 
        error ->
            {"error" , "bad_request"};
        _->
            {_,Items,_,Continuation}  = Result,
            R = case Continuation of
                undefined ->
                    {Items, <<"end">>};
                _->
                    {Items,Continuation}
            end,
            R
    end,
    disconnect(Pid),
    FinalResult.

get_records_next(Bucket, Tag, Type, Amount, Continuation, Criteria)->
    {ok, Pid } = connect(),
    {C , Result} =  case Type of
                        binary -> 
                            riakc_pb_socket:get_index_eq(Pid,
                                list_to_binary(Bucket),
                                {binary_index, Tag},
                                list_to_binary(Criteria),
                                [{max_results, Amount},
                                    {continuation, list_to_binary(Continuation)}])
                    end,
    FinalResult = case C of 
        error ->
            {"error" , "bad_request"};
        _->
            {_,Items,_,Continuation2}  = Result,
            R = case Continuation2 of
                undefined ->
                    {Items, "end"};
                _->
                    {Items, Continuation2}
            end,
            R
    end,
    disconnect(Pid),
    FinalResult.

get_keys(Name)->
    {ok, Pid} = connect(),
    NameBinary = list_to_binary(Name),
    {ok, Keys} = riakc_pb_socket:list_keys(Pid, NameBinary),
    disconnect(Pid),
    Keys.

get_number(Name) ->
    {ok, Pid} = connect(),
    NameBinary =  list_to_binary(Name),
    {ok, Keys} = riakc_pb_socket:list_keys(Pid, NameBinary),
    disconnect(Pid),
    Number = erlang:size(list_to_tuple(Keys)),
    Number.

get_buckets()->
    {ok, Pid} = connect(),
    {ok, Buckets} = riakc_pb_socket:list_buckets(Pid),
    disconnect(Pid),
    Buckets.

delete_bucket(Name, Key) ->
    {ok, Pid} = connect(),
    N = case is_binary(Name) of
        false ->
            list_to_binary(Name);
        true ->
            Name
    end,
    K = case is_binary(Key) of
        false ->
            list_to_binary(Key);
        true ->
            Key
    end,
    riakc_pb_socket:delete(Pid, N, K),
    disconnect(Pid).

set_bucket_json(Bucket, Key, Data) ->
    {ok, Pid} = connect(),
    In = jsx:encode(Data),
    Obj = riakc_obj:new(Bucket, Key, In, <<"application/json">>),
    ok = riakc_pb_socket:put(Pid, Obj),
    disconnect(Pid).

set_bucket(Name, undefined , Data)->
    {ok, Pid} = connect(),
    N = case is_binary(Name) of
        false ->
            list_to_binary(Name);
        true ->
            Name
    end,
    Obj = riakc_obj:new(N, undefined, Data),
    riakc_pb_socket:put(Pid, Obj),
    disconnect(Pid);

set_bucket(Name, Key, Data) ->
    {ok, Pid} = connect(),
    N = case is_binary(Name) of
        false ->
            list_to_binary(Name);
        true ->
            Name
    end,
    K = case is_binary(Key) of
        false ->
            list_to_binary(Key);
        true ->
            Key
    end,
    Obj = riakc_obj:new(N, K, Data),
    
    Result = case riakc_pb_socket:put(Pid, Obj) of
        ok ->
            ok;
        _-> 
            error
    end,
    disconnect(Pid),
    Result.

get_bucket_json(Name, Key) ->
    {ok, Pid} = connect(),
    {ok, Obj}  = riakc_pb_socket:get(Pid, Name, Key),
    Data = riakc_obj:get_value(Obj),
    disconnect(Pid),
    jsx:decode(Data).

check_data_value(Name, Key) ->
    Data = check_data(Name, Key),
    Value = riakc_obj:get_value(Data),
    Value.

update_data_value(Name, Key, NewData) ->
    {ok, Pid} = connect(),
    {ok, Obj}  = riakc_pb_socket:get(Pid, Name, Key),
    NewObj = riakc_obj:update_value(Obj, NewData),
    {ok, _ObjFinal} = riakc_pb_socket:put(Pid, NewObj, [return_body]),
    disconnect(Pid).

update_data_value_json(Name, Key, NewJsonData) ->
    {ok, Pid} = connect(),
    {ok, Obj}  = riakc_pb_socket:get(Pid, Name, Key),
    NewObj = riakc_obj:update_value(Obj, jsx:encode(NewJsonData), <<"application/json">>),
    {ok, _ObjFinal} = riakc_pb_socket:put(Pid, NewObj, [return_body]),
    disconnect(Pid).

check_data(Name, Key) ->
    {ok, Pid} = connect(),
    Res = case riakc_pb_socket:get(Pid, Name, Key) of
            {ok, Obj} -> 
                Obj;
            _-> 
                not_found
    end,
    disconnect(Pid),
    Res.

update_check_bucket(Name, Key, Command, _Value, _Number) ->
    {ok, Pid} = connect(),
    R = case Command of
        check ->
            Result  = riakc_pb_socket:get(Pid, Name, Key),
            case Result of
                {ok, O} ->
                    V =  case _Number of 
                    some ->riakc_obj:get_values(O);
                    one ->binary_to_list(riakc_obj:get_value(O))
                    end,
                    V;
                {error, notfound} ->
                    ""
            end;
        update ->
            set_bucket(Name , Key, list_to_binary(_Value));
        update_value ->
            {ok, O} = riakc_pb_socket:get(Pid, Name, Key),
            ONew = riakc_obj:update_value(O, _Value),
            {ok, OFinal} = riakc_pb_socket:put(Pid, ONew, [return_body]),
            OFinal
    end,
    disconnect(Pid),
    R.


clear_meal_indexes(MealId) ->
    {ok, Pid}  = connect(),
    {ok, Obj}  = riakc_pb_socket:get(Pid, <<"meals">>, MealId),
    MD = riakc_obj:get_metadata(Obj),
    MDu = riakc_obj:clear_secondary_indexes(MD),
    Obju = riakc_obj:update_metadata(Obj, MDu),
    riakc_pb_socket:put(Pid, Obju),
    disconnect(Pid).

update_meal_indexes(Indx, MealId)->
    {ok, Pid}  = connect(),
    {ok, Obj}  = riakc_pb_socket:get(Pid, <<"meals">>, MealId),
    MD = riakc_obj:get_metadata(Obj),
    [   
        _Title,
        _Desc,
        _Ingredients,
        _Portion,
        _Price,
        _Available,
        _Delivery
    ] = Indx,

    %% Get constant indexes
    MealIdIndex = riakc_obj:get_secondary_index(MD, {binary_index, "meal_id"}),
    CookIndex = riakc_obj:get_secondary_index(MD, {binary_index, "cook"}),
    CreatedIndex = riakc_obj:get_secondary_index(MD, {binary_index, "created"}),
    KitchenIndex = riakc_obj:get_secondary_index(MD, {binary_index, "kitchen"}),

    %%Update indexes if
    TitleIndex = case riakc_obj:get_secondary_index(MD, {binary_index, "title"}) of
                    [TitleIdx] ->
                        case TitleIdx of
                               _Title ->
                                   [TitleIdx];
                               [] ->
                                   [_Title];
                               _->
                                   [_Title]
                           end;
                    notfound ->
                        [_Title]
                end,
    DescIndex = case riakc_obj:get_secondary_index(MD, {binary_index, "description"}) of
                    [DescIdx] ->
                        case DescIdx of
                               _Desc ->
                                   [DescIdx];
                               [] ->
                                   [_Desc];
                               _->
                                   [_Desc]
                           end;
                    notfound ->
                        [_Desc]
                end,       
    
     IngredientsIndex = case riakc_obj:get_secondary_index(MD, {binary_index, "ingredients"}) of
                    [IngredientsIdx] ->
                        case IngredientsIdx of
                               _Ingredients ->
                                   [IngredientsIdx];
                               [] ->
                                   [_Ingredients];
                               _->
                                   [_Ingredients]
                           end;
                    notfound ->
                        [_Ingredients]
                end,           
     
    DeliveryIndex = case riakc_obj:get_secondary_index(MD, {binary_index, "delivery"}) of
                    [DeliveryIdx] ->
                        case DeliveryIdx of
                               _Delivery ->
                                   [DeliveryIdx];
                               [] ->
                                   [_Delivery];
                               _->
                                   [_Delivery]
                           end;
                    notfound ->
                        [_Delivery]
                end,           
    
    PriceIndex = case riakc_obj:get_secondary_index(MD, {binary_index, "price"}) of
                    [PriceIdx] ->
                        case PriceIdx of
                               _Price ->
                                   [PriceIdx];
                               [] ->
                                   [_Price];
                               _->
                                   [_Price]
                           end;
                    notfound ->
                        [_Price]
                end,
                
    PortionIndex = case riakc_obj:get_secondary_index(MD, {binary_index, "portion"}) of
                    [PortionIdx] ->
                        case PortionIdx of
                               _Portion ->
                                   [PortionIdx];
                               [] ->
                                   [_Portion];
                               _->
                                   [_Portion]
                           end;
                    notfound ->
                        [_Portion]
                end,           
    
    AvailableIndex = case riakc_obj:get_secondary_index(MD, {binary_index, "available"}) of
                    [AIdx] ->
                        case AIdx of
                               _Available ->
                                   [AIdx];
                               [] ->
                                   [_Available];
                               _->
                                   [_Available]
                           end;
                    notfound ->
                        [_Available]
                end,           
    clear_meal_indexes(MealId),
    {ok, UpObj}  = riakc_pb_socket:get(Pid, <<"meals">>, MealId),
    NewMD = riakc_obj:get_metadata(UpObj),
    
    io:format("Printing ~p ~n",[IngredientsIndex]),
    io:format("Printing ~p ~n",[DescIndex]),
    io:format("Printing ~p ~n",[DeliveryIndex]),
    io:format("Printing ~p ~n",[PriceIndex]),
    io:format("Printing ~p ~n",[PortionIndex]),
    io:format("Printing ~p ~n",[MealIdIndex]),
    io:format("Printing ~p ~n",[KitchenIndex]),
    io:format("Printing ~p ~n",[CookIndex]),
    io:format("Printing ~p ~n",[TitleIndex]),
     io:format("Printing ~p ~n",[AvailableIndex]),
    io:format("Printing ~p ~n",[CreatedIndex]),


    UpdatedIndexes = [
                {{binary_index,"ingredients"},IngredientsIndex},
 		{{binary_index,"description"}, DescIndex},
 		{{binary_index,"delivery"}, DeliveryIndex},
 		{{binary_index,"price"}, PriceIndex},
 		{{binary_index,"portion"}, PortionIndex},
 		{{binary_index,"meal_id"}, MealIdIndex},
 		{{binary_index,"kitchen"}, KitchenIndex},
 		{{binary_index,"cook"}, CookIndex},
 		{{binary_index,"title"}, TitleIndex},
 		{{binary_index,"available"}, AvailableIndex},
 		{{binary_index,"created"}, CreatedIndex}
	],

   io:format("Printing ~p ~n",[UpdatedIndexes]),
   FinalMD = riakc_obj:set_secondary_index(NewMD,UpdatedIndexes),
   ObjFinal = riakc_obj:update_metadata(UpObj, FinalMD),
   ok = riakc_pb_socket:put(Pid, ObjFinal).


json_indexes(Bucket, Indx)->
    [   {_N1,_V1},
        {_N2,_V2},
        {_N3,_V3},
        {_N4,_V4},
        {_N5,_V5},
        {_N6,_V6},
        {_N7,_V7},
        {_N8,_V8},
        {_N9,_V9},
        {_N10,_V10},
        {_N11,_V11}
    ] = Indx,
    Obj1 = riakc_obj:get_update_metadata(Bucket),
    Obj2 = riakc_obj:set_secondary_index(Obj1,
                [{{binary_index, _N1}, [_V1]},
                 {{binary_index, _N2}, [_V2]},
                 {{binary_index, _N3}, [_V3]},
                 {{binary_index, _N4}, [_V4]},
                 {{binary_index, _N5}, [_V5]},
                 {{binary_index, _N6}, [_V6]},
                 {{binary_index, _N7}, [_V7]},
                 {{binary_index, _N8}, [_V8]},
                 {{binary_index, _N9}, [_V9]},
                 {{binary_index, _N10}, [_V10]},
                 {{binary_index, _N11}, [_V11]}
                ]),
    riakc_obj:update_metadata(Bucket,Obj2).


set_bucket_indexed_json(Bucket, Key, Data, Indexes) ->
    {ok, Pid} = connect(),
    In = jsx:encode(Data),
    Obj = riakc_obj:new(Bucket, Key, In, <<"application/json">>),
    IndexedObj = json_indexes(Obj, Indexes),
    Result = riakc_pb_socket:put(Pid, IndexedObj),
    disconnect(Pid),
    Result.

set_indexes(Bucket, Type, _N1, _N2, _N3, _V1, _V2 ,_V3)->
    O1 = riakc_obj:get_update_metadata(Bucket),
    
    O2 = case Type of
        integer ->
            riakc_obj:set_secondary_index(O1,
                [{{integer_index,_N1 }, [_V1]}]);
        binary ->
            riakc_obj:set_secondary_index(O1,
                [{{binary_index, _N1}, [_V1]},
                 {{binary_index, _N2}, [_V2]}]);
        multi->
            riakc_obj:set_secondary_index(O1,
                [{{binary_index, _N1}, [_V1]},
                 {{binary_index, _N2}, [_V2]},
                 {{integer_index, _N3}, [_V3]}])

    end,
    riakc_obj:update_metadata(Bucket,O2).

set_indexed_bucket(Name, Key, Data, 
        Type, Indexes) ->
    {ok, Pid} = connect(),
    N = case is_binary(Name) of
        false ->
            list_to_binary(Name);
        true ->
            Name
    end,
    K = case is_binary(Key) of
        false ->
            list_to_binary(Key);
        true ->
            Key
    end,
    [_N1, _N2, _N3, _V1, _V2, _V3] = Indexes,
    Obj = riakc_obj:new(N, K, Data),
    Obj2 = set_indexes(Obj, Type, _N1, _N2, _N3, _V1, _V2, _V3),
    riakc_pb_socket:put(Pid, Obj2),
    disconnect(Pid).
