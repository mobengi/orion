%% file: src/request_handler.erl
%% --------------------------
-module(request_handler).
-compile({parse_transform, leptus_pt}).

-include("const.hrl").
%% Leptus callbacks
-export([init/3]).
-export([cross_domains/3]).
-export([terminate/4]).

%% Leptus routes
-export([get/3,
        post/3,
	delete/3]).

%% Cross Domain Origin
%% It accepts any host for cross-domain requests
cross_domains(_Route, _Req, State) ->
    {?DOMAINS_LIST, State}.

%% Start
init(_Route, _Req, State) ->
   {ok, State}.

%% Test
get("/api", _Req, State) ->
    %Name = leptus_req:param(Req, name),
    Body = [{<<"message">>, <<"Welcome to the MuncHoodAPI">>}],
    {200, {json, Body}, State};

%% Retreive profile info
get("/profile/:fbid", Req, State) ->
    %% Get ID from query string
    Id = leptus_req:param(Req, fbid),
    case riak_driver:check_data(Id, ?KEY_PROFILE) of
        not_found ->
            ResponseMessage = ?NOT_FOUND,
            {404, {json, ResponseMessage}, State};
        _->
            Response  = unicode:characters_to_binary(jsx:term_to_json(profile_data:get_full_profile(Id))),
            {200, {json, Response}, State}
    end;

%% Retrive photo info
get("/photoinfo/:fbid", Req, State) ->
    %% Get ID from query string
    Id = leptus_req:param(Req, fbid),
    case riak_driver:check_data(Id, ?KEY_PHOTO) of
        not_found ->
            ResponseMessage = ?NOT_FOUND,
            {404, {json, ResponseMessage}, State};
        _->
            Response  = riak_driver:get_bucket_json(Id, ?KEY_PHOTO),
            {200, {json, Response}, State}
    end;

% Retrive meal
get("/meals/:mealid", Req, State) ->
    %% Get ID from query string
    MealId = leptus_req:param(Req, mealid),
    case riak_driver:check_data(<<"meals">>,MealId) of
        not_found ->
            ResponseMessage = ?NOT_FOUND,
            {404, {json, ResponseMessage}, State};
        _->
            Response  = riak_driver:get_bucket_json(<<"meals">>, MealId),
            {200, {json, Response}, State}
    end;

%% Retrive meals by cook
get("/meals/cook/:fbid", Req, State) ->
    %% Get ID from query string
    CookId = leptus_req:param(Req, fbid),
    case riak_driver:check_data(CookId, ?KEY_SIGNUP) of
        not_found ->
            Response = ?NOT_FOUND,
            {404, {json, Response}, State};
        _->
            Response =  meals:get_cook_meals(CookId),
            {200, {json, Response}, State}
    end;

%% Retrive payinfo
get("/payinfo/:fbid", Req, State) ->
    %% Get ID from query string
    Id = leptus_req:param(Req, fbid),
    case riak_driver:check_data(Id, ?KEY_PAYINFO) of
        not_found ->
            ResponseMessage = ?NOT_FOUND,
            {404, {json, ResponseMessage}, State};
        _->
            Response  = riak_driver:get_bucket_json(Id, ?KEY_PAYINFO),
            {200, {json, Response}, State}
    end;

% Get all meals
get("/meals", _Req, State) ->
    %% TODO implement get_all_records function
    Response =  meals:get_meals_with_cook(),
    %io:format("~s~n", [Response]),
    {200, {json, Response}, State}.

%% Sign up
post("/signup", Req, State) ->
   %% Get POST body query string
   Post = leptus_req:body_qs(Req),

   %% Get desired fields from POST
   {<<"fb_id">>, FacebookId} = lists:keyfind(<<"fb_id">>, 1, Post),
   {<<"role">>, Role} = lists:keyfind(<<"role">>, 1, Post),
   Response = profile_data:sign_up(FacebookId, Role, Post),
   {200, {json, Response}, State};

%% Payment info
post("/payinfo/create", Req, State) ->
   %% Get POST body query string
   Post = leptus_req:body_qs(Req),
   
   {<<"fb_id">>, FacebookId} = lists:keyfind(<<"fb_id">>, 1, Post), 
   case riak_driver:check_data(FacebookId, ?KEY_PAYINFO) of
       not_found ->
           PayInfoData = payinfo_data:payinfo_parse_data(FacebookId, Post),
           Response = payinfo_data:save_payinfo(Post, PayInfoData),
           {200, {json, Response}, State};
       _->
           UpdatedPayInfoData = payinfo_data:update_parse_data(Post),
           Response = payinfo_data:update_payinfo(FacebookId , UpdatedPayInfoData, Post),
           {200, {json, Response}, State}
   end;

%% PhotoInfo
post("/photoinfo/create", Req, State) ->
   %% Get POST body query string
   Post = leptus_req:body_qs(Req),
   
   {<<"fb_id">>, FacebookId} = lists:keyfind(<<"fb_id">>, 1, Post), 
   case riak_driver:check_data(FacebookId, ?KEY_PHOTO) of
       not_found ->
           case riak_driver:check_data(FacebookId, ?KEY_SIGNUP) of
                  not_found ->
                      Response = ?NOT_FOUND,
                      {404, {json, Response}, State};
                  _->
                      PhotoData = photo_data:photo_parse_data(FacebookId, Post),
                      Response = photo_data:save_photo(Post, PhotoData),
                      {200, {json, Response}, State}
          end;
       _->
           case riak_driver:check_data(FacebookId, ?KEY_SIGNUP) of
                  not_found ->
                      Response = ?NOT_FOUND,
                      {404, {json, Response}, State};
                  _->
                      UpdatePhotoData = photo_data:update_parse_data(Post),
                      Response = photo_data:update_photo(FacebookId , UpdatePhotoData, Post),
                      {200, {json, Response}, State}
              end
   end;

%% Set profile
post("/profile/create", Req, State) ->
   %% Get POST body query string
   Post = leptus_req:body_qs(Req),

   {<<"fb_id">>, FacebookId} = lists:keyfind(<<"fb_id">>, 1, Post),
   case riak_driver:check_data(FacebookId, ?KEY_PROFILE) of
       not_found ->
           case riak_driver:check_data(FacebookId, ?KEY_SIGNUP) of
               not_found ->
                   Response = ?NOT_FOUND,
                   {404, {json, Response}, State};
               _->
                   ProfileData = profile_data:profile_parse_data(FacebookId, Post),
                   Response = profile_data:save_profile(Post, ProfileData),
                   %% Return success
                   {200, {json, Response}, State}
           end;
        _->
             case riak_driver:check_data(FacebookId, ?KEY_SIGNUP) of
                  not_found ->
                      Response = ?NOT_FOUND,
                      {404, {json, Response}, State};
                  _->
                      UpdateProfileData = profile_data:update_parse_data(Post),
                      Response = profile_data:update_profile(FacebookId ,UpdateProfileData, Post),
                      {200, {json, Response}, State}
              end
   end;

%% List a meal
post("/meal/create", Req, State) ->
   %% Get POST body query string
   Post = leptus_req:body_qs(Req),
   case lists:keyfind(<<"meal_id">>, 1, Post) of
       {<<"meal_id">>, JsonMealId} ->
             TimeStamp = riak_driver:check_data_value(JsonMealId, ?KEY_MEALCREATED),
             CookId = riak_driver:check_data_value(JsonMealId, ?KEY_MEALCOOKID),
             UpdateMealData = meal_data:update_parse_data(Post),
             Response = meal_data:update_meal(JsonMealId, TimeStamp, CookId, UpdateMealData, Post),
             {200, {json, Response}, State};
       false ->
            %%Generate Ids
            {<<"fb_id">>, FacebookId} = lists:keyfind(<<"fb_id">>, 1, Post),
            Meal = <<"munchmeal">>,
            TimeStamp = utils:get_timestamp(),
            CookId = FacebookId,
            MealId= <<Meal/binary, TimeStamp/binary, CookId/binary>>,
            MealData = meal_data:meal_parse_data([Meal, TimeStamp, MealId,FacebookId], Post),
            Response = meal_data:save_meal(Post,MealData),
            %% Return success
            {200, {json, Response}, State}
   end.

%% Delete 
delete("/payinfo/:fbid", Req, State) ->
   Id = leptus_req:param(Req, fbid),
   ok = payinfo_data:delete_payinfo(Id),
   ResponseInfo = [{<<"payinfo">>,[{<<"fb_id">>, Id}]++ ?DELETED}],
   Response = jsx:to_json(ResponseInfo),
   {200, {json, Response}, State};
%% Delete
delete("/profile/:fbid", Req, State) ->
   FacebookId = leptus_req:param(Req, fbid),
   case riak_driver:check_data(FacebookId, ?KEY_PROFILE) of
        not_found ->
            ResponseMessage = ?NOT_FOUND,
            {404, {json, ResponseMessage}, State};
        _->
            ok = profile_data:delete_profile(FacebookId),
            Response = [{<<"fb_id">>, FacebookId}]++?DELETED,
            {200, {json, Response}, State}
    end;

%% Delete
delete("/meal/:meal_id", Req, State) ->
   MealId = leptus_req:param(Req, meal_id),
   ok = meal_data:delete_meal(MealId),
   Response = [{<<"meal_id">>,MealId}]++?DELETED,
   {200, {json, Response}, State};
%% Delete
delete("/photoinfo/:fbid", Req, State) ->
   Id = leptus_req:param(Req, fbid),
   ok = photo_data:delete_photo(Id),
   ResponseInfo = [{<<"photoinfo">>,[{<<"fb_id">>, Id}]++?DELETED}],
   Response = jsx:to_json(ResponseInfo),
   {200, {json, Response}, State}.

%% End
terminate(_Reason, _Route, _Req, _State) ->
   ok.
