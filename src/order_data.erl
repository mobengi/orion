-module(order_data).

-include("const.hrl").
-export([order_parse_data/1,
        save_order/2,
        delete_order/1]).

order_parse_data(Post) ->

    MealId = case lists:keyfind(<<"meal_id">>, 1, Post) of
       {<<"meal_id">>, JsonMealId} ->
           JsonMealId;
       false ->
           <<"">>;
       _->
           <<"">>
    end,
   
    Buyer = case lists:keyfind(<<"buyer_id">>, 1, Post) of
       {<<"buyer_id">>, JsonBuyer} ->
           JsonBuyer;
       false ->
           <<"">>;
       _->
           <<"">>
    end,

    TimeStamp = utils:get_timestamp(),
    OrderId = <<?ORDER/binary, TimeStamp/binary>>,
    
    [
        OrderId,
        MealId,
        TimeStamp,
        Buyer
       
    ]. 

save_order(Json,Data) ->
   [
       OrderId,
       MealId,
       TimeStamp,
       Buyer
   ] = Data,
   riak_driver:set_bucket(OrderId, ?KEY_ORDERTIME, TimeStamp),
   riak_driver:set_bucket(OrderId, ?KEY_ORDERBUYER, Buyer),
   riak_driver:set_bucket(OrderId, ?KEY_ORDERMEALID, MealId),
   
   JsonData =  Json ++ [{<<"order_id">>, OrderId}, {<<"order_time">>, TimeStamp}],
   
   case  riak_driver:set_bucket_json(OrderId, ?KEY_ORDER , JsonData) of
       ok  -> 
           riak_driver:set_bucket_json(?ORDERS, OrderId , JsonData),
           mail_sender:send_email_purchase(MealId, Buyer),
           CookId = riak_driver:check_data_value(MealId, ?KEY_MEALCOOKID),
           EmailCook = binary_to_list(riak_driver:check_data_value(CookId, ?KEY_EMAIL)),
           mail_sender:send_email_order(MealId, EmailCook),
           JsonData;
       _->
           ?NOT_ORDER
   end.

delete_order(OrderId) ->
    ok = riak_driver:delete_buckets(OrderId, all, [], []).
